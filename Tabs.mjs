import { hasChildren, length, setActiveTab, removeActive } from '../utils.mjs'

let Tabs = ({ controle, content, initalTab = 0 }) => {

    let hasError = validate(controle, content, initalTab)
    if (hasError) return


    let contentNodes = Array.from(content.children)
    let renderTab = (index) => {
        contentNodes.forEach((node, nodeIndex) => {
            if (index === nodeIndex) {
                node.style.display = 'block'
            } else {
                node.style.display = 'none'
            }
        })
    }

    let tabs = Array.from(controle.children)
    tabs.forEach((node, index) => {
        node.addEventListener('click', () => {
            removeActive(tabs, node)
            setActiveTab(node)
            renderTab(index)
        })
    })

    setActiveTab(tabs[initalTab])
    renderTab(initalTab)
}

function validate(controle, content, initalTab) {
    let error = false

    if (hasChildren(controle) === false) {
        console.error(`Expected Tab List to have children`)
        error = true
    }

    if (hasChildren(content) === false) {
        console.error(`Expected Tab Content to have children`)
        error = true
    }

    if (length(content) !== length(controle)) {
        let expected = `Expected Tab List and Tab Content to be the same length.`
        let instead = `Instead got Tab List: ${length(controle)} and Tab Content: ${length(content)}`
        console.error(expected, instead)
        error = true
    }

    if (initalTab >= length(content)) {
        console.error(`Expected initalTab to be between 0 and ${length(content)}, insted got: ${initalTab}`)
        error = true
    }

    return error   
}

export default Tabs