import Tabs from './Tabs.mjs'
import { querySelector, hasValue, onSubmit } from './utils.mjs'

Tabs({
    controle: querySelector('#js-tab-list'),
    content: querySelector('#js-tab-content'),
})

let form = querySelector('#js-monsters')

form.addEventListener('submit', async event => {
    event.preventDefault()

    let inputs = Array
        .from(form.querySelectorAll('input'))

    let body = inputs
        .filter(hasValue)
        .map(input => {
            return { [input.name]: input.value }
        })
        .reduce((acc, x) => Object.assign(acc, x), {})

    inputs.forEach(input => input.setAttribute('disabled', ''))
    form.querySelector('button').setAttribute('disabled', '')

    try {
        await onSubmit(body)
        form.classList.add('success')
    } catch (err) {
        inputs.forEach(input => input.removeAttribute('disabled'))
        form.querySelector('button').removeAttribute('disabled')
    }
})
