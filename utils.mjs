
export let length = (node) => {
    return node.children.length
}

export let hasChildren = (node) => {
    return length(node) > 0
}

export let querySelector = (query) =>{
    return document.querySelector(query)
}

export let hasValue = (input) => {
    switch (input.type) {
        case 'radio':
            return input.checked

        case 'text':
        case 'email':
            return Boolean(input.value)

        default:
            console.error('Mismatched type')
    }
}

export let onSubmit = (body) => {
    return new Promise((resolve) => {
        console.log('POST: ', body)
        setTimeout(() => {
            resolve()
        }, 1000)
    })
}

export let setActiveTab = node => {
    node.classList.add('active')
    node.setAttribute('tabindex', 0)
}

export let removeActive = (nodes, node) => {
    nodes.forEach(node => node.classList.remove('active'))
    node.setAttribute('tabindex', -1)
}
